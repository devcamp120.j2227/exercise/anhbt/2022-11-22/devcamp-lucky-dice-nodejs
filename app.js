
const express = require("express");
const app = express();

// khai báo middleware
const middleware = require("./app/middleware/middleware");
app.use("/random-number", middleware.logCurrentTimeMiddleware, middleware.logRequestMethod);

function getRandomInt1to6() {
    return Math.floor(Math.random() * 6 + 1); 
}


app.get("/random-number", function(request, response){
    response.json({
        message: "Random Number Got!",
        data: getRandomInt1to6()
    })
})

app.use(express.static(__dirname + "/views"));
app.get("/", function(request, response) {
    response.sendFile(__dirname + "/views/LuckyDice.html")
})

const port = 8000;
app.listen(port, () => {
    console.log("App listening on port:", port);
})