const logCurrentTimeMiddleware = function(request, response, next) {
    var currentDate = new Date(); 
    var currentTime = "Time: "
                    + currentDate.getHours() + "h"  
                    + currentDate.getMinutes();
    console.log(currentTime);
    next();
}

const logRequestMethod = function(request, response, next) {
    let requestMethod = request.method;
    console.log("Method: ", requestMethod);
    next();
}
module.exports = {
    logCurrentTimeMiddleware,
    logRequestMethod
}
